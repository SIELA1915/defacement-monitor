# Defacement monitoring tool

## Installation
Most dependencies are contained in the Pipfile, so they can be installed using `pipenv install` in the main directory. However [opencv-python](https://github.com/opencv/opencv-python) failed to install on my machine. So either do `pipenv install opencv-python` if that works for you, otherwise do a manual wheel build as explained on the opencv-python website.

## Running the tool
`defacement_server.py` runs two threads, one acts as a webserver on port 8080, serving the `defacement.html` that is the user interface.
The other thread screenshots the list of websites one by one infinitely while sending image diffs over websocket to the clients having `defacement.html` open.
