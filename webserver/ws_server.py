import websockets
import asyncio
from datetime import datetime
import base64

ws_list = set()
q = None
loop = None
websites = []
latest_time = {}

async def server_handler(websocket):
    global ws_list
    global alert_id
    ws_list.add(websocket)
    try:
        msg = await websocket.recv()
        await websocket.send(msg)
        for i,website in enumerate(websites):
            for uri in website['uris']:
                uri_path = f"{website['base_url']}/{uri}"
                if uri_path[-1] == '/':
                    uri_path = uri_path[:-1]
                img_path = f'{IMAGE_BASE_PATH}/{website["name"]}/{uri}'
                diff_img_path = f'{IMAGE_BASE_PATH}/{website["name"]}/{uri}/diff.png'
                try:
                    with open(diff_img_path, "rb") as image_file:
                        await websocket.send(f"pushImg {i} {datetime.now().timestamp()} {base64.b64encode(image_file.read()).decode('ascii')} {uri_path}")
                except FileNotFoundError as e:
                    await websocket.send(f"pushImg {i} 0 iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEklEQVR42mNk+M+AFzCOKgADALyGCQGyq8YeAAAAAElFTkSuQmCC {uri_path}")
        queue_get = asyncio.create_task(q.get())
        msg_get = asyncio.create_task(websocket.recv())
        done, pending = await asyncio.wait((queue_get, msg_get,), return_when=asyncio.FIRST_COMPLETED)
        while True:
            if queue_get in done:
                message = queue_get.result()
                websockets.broadcast(ws_list, message)
                q.task_done()
                queue_get = asyncio.create_task(q.get())
                pending.add(queue_get)
            if msg_get in done:
                message = msg_get.result()
                websockets.broadcast(ws_list, message)
                msg_get = asyncio.create_task(websocket.recv())
                pending.add(msg_get)
            done, pending = await asyncio.wait(pending, return_when=asyncio.FIRST_COMPLETED)
    except websockets.exceptions.ConnectionClosedOK:
        pass
    finally:
        # Unregister.
        ws_list.remove(websocket)
        await websocket.close()

        
async def start_server():
    global loop
    global q
    loop = asyncio.get_running_loop()
    q = asyncio.Queue()
    async with websockets.serve(server_handler, "", 4321):
        await asyncio.Future()


async def push_image(imgId, changed, base64, name):
    msg = f"pushImg {imgId} {changed} {base64} {name}"
    await q.put(msg)

if __name__ == "__main__":
    asyncio.run(start_server(), debug=True)
