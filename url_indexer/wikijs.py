import certifi
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json

WIKIJS_API_URL = "https://wiki.example.com/graphql"

def markdown2dict(inp):
    '''Code by Kuldeen Singh Sidhu, 2021 on Stack Overflow (CC BY-SA 4.0, https://stackoverflow.com/a/66185947), modified to return python object not json string'''
    lines = inp.split('\n')
    ret=[]
    keys=[]
    for i,l in enumerate(lines):
        if i==0:
            keys=[_i.strip() for _i in l.split('|')]
        elif i==1: continue
        else:
            ret.append({keys[_i]:v.strip() for _i,v in enumerate(l.split('|')) if  _i>0 and _i<len(keys)-1})
    return ret

try:
    test = requests.get(WIKIJS_API_URL)
except requests.exceptions.SSLError as err:
    cafile = certifi.where()
    with open('../certificate.pem', 'rb') as infile:
        customca = infile.read()
    with open(cafile, 'ab') as outfile:
        outfile.write(customca)


def get_index():
    retry_strategy = Retry(
        total=3,
        backoff_factor=1,
        status_forcelist=[429, 500, 502, 503, 504],
        allowed_methods=["HEAD", "GET", "OPTIONS", "POST"]
    )
    adapter = HTTPAdapter(max_retries=retry_strategy)
    httpSess = requests.Session()
    httpSess.mount("https://", adapter)
    httpSess.mount("http://", adapter)

    with open('url_indexer/wikijs_token.txt', 'r') as token_file:
        token = token_file.readline().rstrip('\n')

    api_headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': f"Bearer {token}",
    }

    request_json = json.loads(open(f'url_indexer/wikijs-single-page.json', 'r').read().replace('\n', '').replace('\t', ' '))
    request_json['variables']['id'] = 1732
#    request_json['variables']['id'] = 1735

    response = httpSess.post(WIKIJS_API_URL, headers=api_headers, data=json.dumps(request_json))

    res = []
    
    if response.json().get('errors'):
        print(response.json())
    else:
        resp_json = response.json()['data']['pages']['single']
        pot_table_parts = resp_json['content'].split('\n\n')
        for pot_table in pot_table_parts:
            if "| Zone | Domain | IPv4 | IPv6 |" in pot_table:
                website_list = markdown2dict(pot_table)
                for website in website_list:
                    res.append({'base_url': f"http{'s' if not '80' in website['Domain'] else ''}://{website['Domain']}", 'zone': website['Zone'], 'uris': [''], 'name': '-'.join(website['Domain'].split('.'))})

    return res

    
