import json

def markdown2dict(inp):
    '''Code by Kuldeen Singh Sidhu, 2021 on Stack Overflow (CC BY-SA 4.0, https://stackoverflow.com/a/66185947), modified to return python object not json string'''
    lines = inp.split('\n')
    ret=[]
    keys=[]
    for i,l in enumerate(lines):
        if i==0:
            keys=[_i.strip() for _i in l.split('|')]
        elif i==1: continue
        else:
            ret.append({keys[_i]:v.strip() for _i,v in enumerate(l.split('|')) if  _i>0 and _i<len(keys)-1})
    return ret

def get_index():
    res = []

    with open(f'websites.md', 'r') as f:
        website_list = markdown2dict(f.read().rstrip())
        for website in website_list:
            print(website)
            res.append({'base_url': f"http{'s' if not '80' in website['Domain'] else ''}://{website['Domain']}", 'zone': website['Zone'], 'uris': [''], 'name': '-'.join(website['Domain'].split('.'))})

    return res

    
