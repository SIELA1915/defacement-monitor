#!/usr/bin/env python3
import asyncio

import itertools
import json
import os
from datetime import datetime, timedelta
import time
import base64

import cv2
import numpy as np

from selenium import webdriver

import threading
import http.server
import socketserver
import webserver.ws_server as WSServer

#from url_indexer.wikijs import *
from url_indexer.markdown_file import *

from pathlib import Path

IMG_BASE_PATH = "images"
block_time = []
FADEOUT_FACTOR = 0.1
ACCENT_FACTOR = 1.5

def sendImage(imgId, changed, base64, name):
     future = asyncio.run_coroutine_threadsafe(WSServer.push_image(imgId, changed, base64, name), WSServer.loop)
     future.result()

def main():
    from selenium.webdriver.chrome.options import Options
    chrome_options = Options()
    #chrome_options.add_argument("--disable-extensions")
    #chrome_options.add_argument("--disable-gpu")
    #chrome_options.add_argument("--no-sandbox") # linux only
    chrome_options.add_argument("--disable-dev-shm-usage");
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--window-size=1920,1080")
    # chrome_options.headless = True # also works
    driver = webdriver.Chrome(options=chrome_options)

    for i,website in itertools.cycle(enumerate(WSServer.websites)):
        with webdriver.Chrome(options=chrome_options) as driver:
            driver.set_page_load_timeout(10)
            if datetime.now() - block_time[i] > timedelta(minutes=1):
                block_time[i] = datetime.now()
                for uri in website['uris']:
                    uri_path = f"{website['base_url']}/{uri}"
                    if uri_path[-1] == '/':
                         uri_path = uri_path[:-1]
                    try:
                        driver.get(uri_path)
                        img_path = f'{IMG_BASE_PATH}/{website["name"]}/{uri}'
                        new_time = datetime.utcnow().replace(microsecond=0).isoformat().replace(':', '')
                        new_img_path = f"{img_path}/{new_time}.png"
                        driver.save_screenshot(new_img_path)
                        old_time = WSServer.latest_time[img_path]
                        if old_time == 'latest':
                            continue
                        WSServer.latest_time[img_path] = new_time
                        old_png = cv2.imread(f"{img_path}/{old_time}.png")
                        new_png = cv2.imread(new_img_path)
                        #                    diff_png = new_png.copy()
                        #                    diff_png = diff_png.astype('int8')
                        #                    cv2.absdiff(old_png, new_png, diff_png)
                        #                    diff_png = np.where(diff_png*ACCENT_FACTOR < diff_png, 255, diff_png * ACCENT_FACTOR)
                        diff_png = old_png.astype('int16') - new_png.astype('int16')
                        diff_png[:,:,2] = diff_png[:,:,2] * ACCENT_FACTOR
                        gray = cv2.cvtColor(old_png, cv2.COLOR_BGR2GRAY) * FADEOUT_FACTOR
                        gray = gray.astype('uint8')
                        #gray = np.where(gray < fadeout_amount, 0, gray-fadeout_amount)
                        diff_png = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR) + diff_png
                        diff_png.astype('uint8')
                        diff_img_path = f'{img_path}/diff.png'
                        cv2.imwrite(diff_img_path, diff_png)
                        Path(new_img_path).touch()
                        with open(diff_img_path, "rb") as image_file:
                            sendImage(i, datetime.now().timestamp(), base64.b64encode(image_file.read()).decode('ascii'), uri_path)
                    except Exception as e:
                        if any([errmsg in repr(e) for errmsg in ['ERR_CONNECTION_RESET', 'ERR_CONNECTION_REFUSED', 'ERR_CONNECTION_CLOSED', 'timeout', 'SSL']]):
                            print(f"Error connecting to {website['name']}")
                        else:
                            print(e)
            else:
                 time.sleep(10)
                                    

def newest(path):
    files = os.listdir(path)
    paths = [os.path.join(path, basename) for basename in files]
    if len(paths) > 0:
        return max(paths, key=os.path.getctime)
    else:
        return 'latest'

def http_serve_forever():
    PORT = 8080
    class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):
        def do_GET(self):
            self.path = f"webserver/{self.path.split('/')[-1]}"
            return http.server.SimpleHTTPRequestHandler.do_GET(self)
        def log_message(self, format, *args):
            pass
        
    import socket
        
    with socketserver.TCPServer(("", PORT), MyHttpRequestHandler, bind_and_activate=False) as httpd:
        httpd.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        httpd.server_bind()
        httpd.server_activate()
        print("serving at port", PORT)
        httpd.serve_forever()



if __name__ == "__main__":    
    for website in get_index():
         WSServer.websites.append(website)
         print(f"{website['base_url']}")
         block_time.append(datetime.now()-timedelta(minutes=1))
         for uri in WSServer.websites[-1]['uris']:
              path = f"{IMG_BASE_PATH}/{WSServer.websites[-1]['name']}/{uri}"
              os.makedirs(path, exist_ok=True)
              latest = newest(path).split('/')[-1].rsplit('.', 1)[0]
              WSServer.latest_time[path] = latest

                        
    try:
        thread1 = threading.Thread(name='webserver', target=http_serve_forever).start()
        thread2 = threading.Thread(name='selenium', target=main).start()
        asyncio.run(WSServer.start_server())
    except KeyboardInterrupt:
        os._exit(1)
